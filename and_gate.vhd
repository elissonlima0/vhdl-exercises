library IEEE;

use IEEE.STD_LOGIC_1164.ALL;

ENTITY and_gate IS

port (A,B: IN BIT;
		Y: OUT BIT);
		
END and_gate;

ARCHITECTURE porta_and OF and_gate IS
BEGIN
	Y <= A AND B;
END porta_and;