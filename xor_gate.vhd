
library IEEE;

USE IEEE.STD_LOGIC_1164.ALL;

ENTITY xor_gate IS

port ( A, B : IN BIT;
		 C : OUT BIT );
END xor_gate;

ARCHITECTURE Behavioral of xor_gate IS
	SIGNAL X : BIT;
	SIGNAL Y : BIT;
	
BEGIN

		X <= A AND NOT B;
		Y <= B AND NOT A;
		C <= X OR Y;
		
END Behavioral;
	